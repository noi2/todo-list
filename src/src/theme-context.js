import React from 'react';

export const themes = {
  light: {
    background: '#708090',
  },
  dark: {
    background: '#141E30',
  },
};

export const ThemeContext = React.createContext(themes.dark);
