import React from 'react';
import '../index.css';

class InputField extends React.Component {
  constructor(props) {
    super(props);
  }

  render() {
    const {type, input, inputHandler} = this.props;

    return (
      <input
        placeholder="Введите название задачи"
        type={type}
        value={input}
        onChange={inputHandler}
      />
    );
  }
}

export default InputField;
