import React from 'react';

import Button from './Button';
import DropDown from './Selector';
import {LOW_PRIORITY, URGENT_PRIORITY} from '../consts';

class ListItem extends React.Component {
  constructor(props) {
    super(props);
  }

  getSpanClassName = () => {
    const {priority} = this.props;
    let spanClassName = 'mid';

    if (priority === LOW_PRIORITY) spanClassName = 'low';
    else if (priority === URGENT_PRIORITY) spanClassName = 'high';

    return spanClassName;
  };

  render() {
    const {
      taskId,
      name,
      onEdit,
      onClose,
      isDone,
      onDelete,
      editPriorityHandler,
      priority,
    } = this.props;

    return (
      <tr>
        <td>
          <span className={this.getSpanClassName()} />
        </td>
        <td>{taskId}</td>
        <td>{name}</td>
        <td>
          <Button onClick={() => onEdit(taskId, name)} name="Изменить" />
        </td>
        <td>
          <Button
            onClick={() => onClose(taskId)}
            name={isDone ? 'Не выполнено' : 'Выполнено'}
          />
        </td>
        <td>
          <Button onClick={() => onDelete(taskId)} name="Удалить" />
        </td>
        <td>
          <DropDown
            onChange={editPriorityHandler}
            priority={priority}
            taskId={taskId}
          />
        </td>
      </tr>
    );
  }
}

export default ListItem;
