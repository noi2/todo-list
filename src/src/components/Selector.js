import React from 'react';
import {TASK_PRIORITIES} from '../consts';

class DropDown extends React.Component {
  constructor(props) {
    super(props);
  }

  render() {
    return (
      <select
        id="dropdown"
        defaultValue={this.props.priority}
        onChange={(e) => {
          this.props.onChange(this.props.taskId, Number(e.target.value));
        }}>
        {TASK_PRIORITIES.map((el) => {
          return (
            <option key={el.key} value={el.key}>
              {el.value}
            </option>
          );
        })}
      </select>
    );
  }
}

export default DropDown;
