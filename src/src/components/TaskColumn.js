import ListItem from './ListItem';
import React from 'react';
import withLoader from '../HOCs/IsLoadingHOC';

class TaskColumn extends React.Component {
  constructor(props) {
    super(props);
  }

  render() {
    return (
      <>
        <h2>{this.props.name}</h2>
        <table>
          <thead>
            <tr>
              <th />
              <th>ID</th>
              <th>Наименование</th>
              <th />
              <th />
              <th />
              <th>Приоритет</th>
            </tr>
          </thead>
          <tbody>
            {this.props.list.map((el) => {
              return (
                <ListItem
                  key={el.taskId}
                  name={el.name}
                  taskId={el.taskId}
                  isDone={el.isDone}
                  priority={el.priority}
                  onDelete={this.props.deleteHandler}
                  onEdit={this.props.editHandler}
                  onClose={this.props.closeTaskHandler}
                  editPriorityHandler={this.props.editPriorityHandler}
                />
              );
            })}
          </tbody>
        </table>
      </>
    );
  }
}

export default TaskColumn;
export const TaskColumnWithLoading = withLoader(TaskColumn);
