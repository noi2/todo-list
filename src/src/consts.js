export const LOW_PRIORITY = 1;
export const MEDIUM_PRIORITY = 2;
export const URGENT_PRIORITY = 3;

export const TASK_PRIORITIES = [
  {key: LOW_PRIORITY, value: 'Низкий'},
  {key: MEDIUM_PRIORITY, value: 'Средний'},
  {key: URGENT_PRIORITY, value: 'Срочный'},
];
