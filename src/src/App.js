import React from 'react';
import './App.css';
import InputField from './components/InputField';
import Button from './components/Button';
import './index.css';
import {MEDIUM_PRIORITY} from './consts';
import TaskColumn, {TaskColumnWithLoading} from './components/TaskColumn';
import {ThemeContext, themes} from './theme-context';

class ToDoList extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      input: '',
      taskId: 0,
      list: [],
      isEdit: false,
      tempID: 0,
      isDone: false,
      priority: MEDIUM_PRIORITY,
      theme: themes.dark,
    };
  }

  static contextType = ThemeContext;

  toggleTheme = () => {
    this.setState((state) => ({
      theme: state.theme === themes.dark ? themes.light : themes.dark,
    }));
  };

  inputHandler = (e) => {
    this.setState({input: e.target.value});
  };

  submitHandler = (e) => {
    e.preventDefault();

    const {input} = this.state;

    if (!input) return;

    if (input.length > 100) return alert('Слишком длинное наименование задачи');

    const {list, taskId, isDone, priority} = this.state;
    const tasksDuplicates = list.filter((el) => el.name === input);

    if (tasksDuplicates.length)
      return alert('Такая задача уже присутствует, измените наименование');

    this.setState({
      loading: true,
    });

    setTimeout(() => {
      this.setState({loading: false});
    }, 1000);

    this.setState((prevState) => ({
      taskId: prevState.taskId + 1,
      list: [
        ...list,
        {name: input, taskId: taskId, isDone: isDone, priority: priority},
      ],
      input: '',
    }));
  };

  updateHandler = () => {
    const {input, list, tempID} = this.state;

    const tasksDuplicates = list.filter(
      (el) => el.name === input && el.taskId !== tempID,
    );

    if (tasksDuplicates.length)
      return alert('Такая задача уже присутствует, измените наименование');

    const taskIndex = list.findIndex((task) => task.taskId === tempID);

    list[taskIndex].name = input;

    this.setState({list: list, isEdit: false, input: ''});
  };

  deleteHandler = (taskID) => {
    this.setState(() => {
      const todos = this.state.list.filter((el) => el.taskId !== taskID);

      return {list: todos};
    });
    this.setState({input: '', isEdit: false});
  };

  closeTaskHandler = (taskID) => {
    const list = this.state.list;

    const taskIndex = list.findIndex((task) => task.taskId === taskID);
    list[taskIndex].isDone = !list[taskIndex].isDone;

    this.setState({list: list});
  };

  editHandler = (taskId, name) => {
    this.setState({input: name, isEdit: true, tempID: taskId});
  };

  editPriorityHandler = (taskID, priority) => {
    const list = this.state.list;

    const taskIndex = list.findIndex((task) => task.taskId === taskID);
    list[taskIndex].priority = priority;

    this.setState({list: list});
  };

  render() {
    const {isEdit} = this.state;

    return (
        <div className={'column__flex'} style={this.state.theme}>
          <h2>Сменить цветовую тему этого блока</h2>
          <Button
              onClick={this.toggleTheme}
              name={this.state.theme === themes.dark ? 'Светлая' : 'Тёмная'}
          />
        <h1>Список дел (задач)</h1>
        <div>
          <InputField
            type="text"
            input={this.state.input}
            inputHandler={this.inputHandler}
          />
          <Button
            onClick={isEdit ? this.updateHandler : this.submitHandler}
            name={isEdit ? '✎' : '+'}
          />

          <TaskColumnWithLoading
            name="Активные задачи"
            list={this.state.list.filter((el) => el.isDone === false)}
            deleteHandler={this.deleteHandler}
            editHandler={this.editHandler}
            closeTaskHandler={this.closeTaskHandler}
            editPriorityHandler={this.editPriorityHandler}
          />
          <TaskColumn
            name="Выполненные задачи"
            list={this.state.list.filter((el) => el.isDone === true)}
            deleteHandler={this.deleteHandler}
            editHandler={this.editHandler}
            closeTaskHandler={this.closeTaskHandler}
            editPriorityHandler={this.editPriorityHandler}
          />
        </div>
      </div>
    );
  }
}

ToDoList.contextType = ThemeContext;

export default ToDoList;
