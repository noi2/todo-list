# Todo List project

Проект списка задач (дел) в рамках прохождения курса Creative.ReactJS

## Запуск проекта

`cd src && npm start`

## Запуск линтеров

`cd src && ./node_modules/.bin/eslint ./`
